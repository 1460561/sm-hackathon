<?php
	if(isset($_GET['action']))
	{
		$action = $_GET['action'];
	}else{
		$action = null;
	}
	session_start();

	switch($action) {
		case 'home':
			include __DIR__.'/controllers/homeController.php';
			break;
		case 'audio':
			include __DIR__.'/controllers/homeControllerAudio.php';
			break;
		case 'login':
			include __DIR__.'/controllers/login.php';
			break;
		case 'login-test':
			if(isset($_SESSION['niu'])) {
				include __DIR__.'/controllers/homeController.php';
			} else{
				include __DIR__.'/controllers/login.php';
			}	
			break;
		case 'post-ajax':
			include __DIR__.'/controllers/postAjax.php';
			break;
		case 'post-ajax-audio':	
			include __DIR__.'/controllers/postAjaxAudio.php';
			break; 
		case 'post-ajax-login':
			include __DIR__.'/controllers/postAjaxLogin.php';
			break;
		case 'logout':
			include __DIR__.'/controllers/logoutController.php';
			break;
		case 'post-text':
			include __DIR__.'/controllers/postText.php';
			break;
		case 'post-ajax-start':
			include __DIR__.'/controllers/postStartPresential.php';	
			break;
		default:
			if(isset($_SESSION['niu'])) {
                                include __DIR__.'/controllers/homeController.php';
                        } else{
                                include __DIR__.'/controllers/login.php';
                        } 
			break;
	}
?>
