<!DOCTYPE html>
<html>
<head>
  <title><?php echo $title ?></title>
  <link rel="shortcut icon" href="https://www.google.com/favicon.ico" type="image/x-icon">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</head>
<style>
body  {
	background-image: url("https://www.toutpourlamoto.fr/images/greetings/light-grey-curves-15195-flip.jpg");
	background-size: cover;
	background-repeat: no-repeat;
}
</style>
<body>

<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
?>

<canvas id="canvas" width="640" height="480" style="display: none"></canvas>


<div class="video-wrap" style="display: block;">
	<video id="video" playsinline autoplay></video>
</div>

<br/><br/>

<div id="detector" style="display: block;"></div>   <!-- detector persona -->

<div id="all" style="display: block;">
	<div id="agree" style="display: block;">
		<div id="buttonStart" style="display: none;" onclick= "startPres()">
			<br/>
			<div id="show">
				<div id="avis">Haz click en "Empezar examen" cuando estés preparado. Una vez empezado no podrás moverte de la pantalla. Si en algún momento deja de detectar al alumno, no se podrá enviar el examen. Para enviar la respuesta, presiona el botón grabar y cuando acabes, el botón enviar.</div>
			</div><!-- instruccions examen -->
			<br/> 
			<button id="pressButton" onclick="startExam()">Empezar examen</button>
		</div>
	</div>

	<br/>
	<br/>
	<div id="full-exam" style="display: none"> 
		<div id="examStarted"></div>  <!-- Respon: (necessari) -->

		<div id="examContent">
			<b>
				<u>EXAMEN</u>
			</b> 
			<br/>
			<br/> 
			<b>PREGUNTA 1: ¿Cuales son ácidos ribonucleicos?</b>
			<br/> 
			a)Ninguno 
			<br/>
			b)Alguno 
			<br/>
			c)La a) y la b) 
			<br/>
			d)Ninguna es correcta
			<br/>
			<br/>
			<b>PREGUNTA 2: ¿Cuantos dedos tienes en la mano?</b>
                        <br/>
                        a)4
                        <br/>
                        b)5
                        <br/>
                        c)No lo se
                        <br/>
                        d)3
                        <br/>
			<br/>
			<b>PREGUNTA 3: ¿Cuantos dias llevas encerrado en casa?</b>
                        <br/>
                        a)No suficientes
                        <br/>
                        b)50
                        <br/>
                        c)30
                        <br/>
                        d)He perdido la cuenta
                        <br/>
			<br/>
			<b>PREGUNTA 4: ¿Como te llamas?</b>
                        <br/>
                        a)Marc
                        <br/>
                        b)Joan
                        <br/>
                        c)Oriol
                        <br/>
                        d)Murci
                        <br/>
			<br/>
			<div id="startbuttontest" style="display:block;">
                        	<button type="button" onClick="record('startbuttontest','stopbuttontest','sendbuttontest','texttest','startbuttondes')">Iniciar</button>
                	</div>
			<div id="stopbuttontest" style="display:none;">
                        	<button type="button" onclick="record.stop()">Parar</button>
                	</div>
			<div id="sendbuttontest" style="display:none;">
                		<button type="button" onclick="enviar('test','texttest','startbuttontest','sendbuttontest','startbuttondes')">Enviar</button>
        		</div>
			<div id="texttest" style="display: block;"></div>
			<br/>
			<b>PREGUNTA A DESARROLLAR: </b> 
			<br/>
			- ¿Cual es la diferencia entre un cluster i un grid?
			<br/>
			<div id="startbuttondes" style="display:block;">
                		<button type="button" onClick="record('startbuttondes','stopbuttondes','sendbuttondes','textdes','startbuttontest')">Iniciar</button>
        		</div>
        		<div id="stopbuttondes" style="display:none;">
                		<button type="button" onclick="record.stop()">Parar</button>
        		</div>
        		<div id="sendbuttondes" style="display:none;">
                		<button type="button" onclick="enviar('des','textdes','startbuttondes','sendbuttondes','startbuttontest')">Enviar</button>
        		</div>
        		<div id="textdes" style="display: block;"></div>
		</div>
	</div>
</div>

<div id="result" style="display: none">
	<br/>
	<br/>
	<big>
		<b>	
			<div id="final1"></div>  <!-- final enviar examen -->
		</b>
	</big>
	<big>
		<b>	
			<div id="final2"></div>  <!-- final examen cancelat -->
		<b>
	</big>
</div>
<script>
	var answer = 0;

	function enviar(id_pregunta,text,start,send,object1){
        	
        	var text = document.getElementById(text);
        	text = text.textContent;
        	
		$.ajax({        
                	url: "?action=post-text",
                        type: "POST",
                        data: {text:text, id_pregunta:id_pregunta, answer:answer}

                })

		if(answer === 1){
                	document.getElementById(object1).style.display = "none";
                	document.getElementById(start).style.display = "none";
                	document.getElementById(send).style.display = "none";        		
			
			//NEW
			var final1 = document.getElementById("final1");
			document.getElementById("all").style.display = "none"; 
                	final1.innerHTML = 'EXAMEN ENVIADO Y FINALIZADO';
			document.getElementById("result").style.display = "block";
		}else{
                	answer++;
                	document.getElementById(object1).style.display = "block";
                	document.getElementById(start).style.display = "none";
                	document.getElementById(send).style.display = "none";
        	}
       
	}

	function record(start,stop,send,text,object1) {
        	document.getElementById(object1).style.display = "none";    
        	var stopbutton = document.getElementById(stop);
        	var startbutton = document.getElementById(start);
        	var sendbutton = document.getElementById(send);
        	var text = document.getElementById(text);
        
        	stopbutton.style.display = "block";
        	
		navigator.mediaDevices.getUserMedia({ audio: true })
        	.then(stream => {
                	const mediaRecorder = new MediaRecorder(stream);
                	mediaRecorder.start();
        
                	const audioChunks = [];

                	mediaRecorder.addEventListener("dataavailable", event => {
                        	audioChunks.push(event.data);
                	});
                
                	mediaRecorder.addEventListener("stop", () => {
                        	const audioBlob = new Blob(audioChunks);
                
                        	var reader = new FileReader();
                         	reader.readAsDataURL(audioBlob); 
                         	reader.onloadend = function() {
                         	var base64data = reader.result;                
                        	$.ajax({
                                	url: "?action=post-ajax-audio",
                                	type: "POST",
                                	data: {audios:base64data},
                                	success:function(response)
                                	{
						text.innerHTML = response;
                                	}
                        	})
                	}        
                	});
                        function stop() {
                        	mediaRecorder.stop() ;
                        	stopbutton.style.display = "none";
                        	startbutton.style.display = "block";
                        	text.style.display = "block";
				sendbutton.style.display = "block";
                        }
                        record.stop = stop;

		});
		startbutton.style.display = "none";
		text.innerHTML = "";
	}
</script>

<script>
	function startExam() {
		document.getElementById("full-exam").style.display = "block";
		document.getElementById("agree").style.display = "none";
		document.getElementById("examStarted").innerHTML = 'Responde:';
	}	

	function startPres() {
		$.ajax({
                url: "?action=post-ajax-start",
                type: "POST",
                data: {text:'hola'}
                                        

		})
	}


</script>

<script>
'use strict';

const video = document.getElementById('video');
const canvas = document.getElementById('canvas');
const snap = document.getElementById('snap');
const errorMsgElement = document.getElementById('spanErrorMsg');

const constraints = {
  audio: false,
  video: {
    width: 360, height: 200
  }
};

async function init(){
  try{
    const stream = await navigator.mediaDevices.getUserMedia(constraints);
    handleSuccess(stream);
  }
  catch(e){
    //errorMsgElement.innerHTML = `navigator.getUserMedia.error:${e.toString()}`;
  }
}

function handleSuccess(stream) {
    window.stream = stream;
    video.srcObject = stream;
}

init();

var context = canvas.getContext('2d');

window.setInterval(function() {
 
 context.drawImage(video, 0, 0, 640, 480);
 var image = document.getElementById("canvas").toDataURL();


  $.ajax({
	url: "?action=post-ajax",
	type:"POST",
	data: {images:image},
	success:function(response)
	{
		var started = document.getElementById("examStarted");
		var detector = document.getElementById("detector");
		var buttonStart = document.getElementById("buttonStart");
		var show = document.getElementById("show");
		var final1 = document.getElementById("final1");
		
		if(response == "SI"){
			detector.innerHTML = 'PERSONA DETECTADA';
			buttonStart.style.display = "block";
			show.style.display = "block";
		}else{
			detector.innerHTML = 'PERSONA NO DETECTADA';
			buttonStart.style.display = "none";
			show.style.display = "none";
		}
		if(started.textContent === 'Responde:'){
			if(detector.textContent === 'PERSONA NO DETECTADA' && final1.textContent != 'EXAMEN ENVIADO Y FINALIZADO'){ 
				document.getElementById("final2").innerHTML = 'EXAMEN FINALIZADO (NO SE HA DETECTADO NINGUNA PERSONA MIENTRAS SE REALIZABA EL EXAMEN)';
				buttonStart.style.display = "none";
				started.style.display = "none";
                		document.getElementById("examContent").style.display = "none";
				show.style.display = "none";
				final1.style.display = "none";
				document.getElementById("result").style.display = "block";
			} 		
		}
					
 	}	
	})
;},5000);

</script>
</body>
</html>
