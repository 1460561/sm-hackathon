<!DOCTYPE>
<html>
	<head>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<title><?php echo $title ?></title>
	</head>
	<body>
		<h1>Login</h1>

		<form id="login" action="?action=login-test" method="POST" class="login-form">
			<label>NIU:</label>
			<input type="text" name="niu" required/>

			<label>Password:</label>
  			<input type="password" name="password" required/>

  			<input type="submit" value="Login">
		</form>
		<?php 
		if(isset($errors)) {
			foreach ($errors as $error) {
			?>
			<p><?php echo $error ?></p>
			<?php
			}
		}
		?>
	</body>
</html>
