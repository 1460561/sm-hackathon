<?php
	include __DIR__.'/../config/firebase.php';

	function compare($niu, $password){
        	$usuari = new Users("usuaris");
		$response = [];
		$password_hashed = $usuari->get($niu);		
		
		$response['niu'] = $niu;
		
		if(password_verify($password, $password_hashed)) {
			 $response['islogged'] = 'true'; 
		} else {
			$response['islogged'] = 'false'; 
		}
		
		return $response;

	}
?>
