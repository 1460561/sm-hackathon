<?php 
	$title = "LOGIN";
	$errors = [];
	$logged = false;

	if($_SERVER['REQUEST_METHOD'] === 'POST') {
		$niu = $_POST["niu"];
		$passwd = $_POST["password"];

		if(empty($niu)) {
			$errors["niu"] =  "Error: El camp NIU esta buit";
		} else if(empty($passwd)) {
			$errors["password"] = "Error: El camp Password esta buit";
		}
		
		if(empty($errors)) {
			require_once __DIR__."/../models/login.php";
			$logged = compare($niu, $passwd);
		}else {
			require_once __DIR__."/../views/login.php";
		}

		if($logged['islogged'] === 'true') {
			$_SESSION['niu'] = $logged['niu'];	
			require_once __DIR__."/../views/home.php";
		} else {
			$errors["wrong_credentials"] = "Error: El camp del NIU o la contraseña no son correctes";	
			include __DIR__.'/../views/login.php';
		}
	} else {
		include __DIR__.'/../views/login.php';
	}

	

?>
