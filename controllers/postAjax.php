<?php
	namespace Google\Cloud\Samples\Vision;
    	require "/var/www/html/vendor/autoload.php";
    	use Google\Cloud\Vision\V1\ImageAnnotatorClient;
		
	if(isset($_POST['images'])){
		$niu = $_SESSION["niu"];
		putenv('GOOGLE_APPLICATION_CREDENTIALS=/var/www/html/sm-hackathon-project-e820d9f88948.json');
		$img = $_POST['images'];
		list($type, $img) = explode(';', $img);
		list(, $img)      = explode(',', $img);
		$img = base64_decode($img);

		$fileName = "/var/www/html/data/image" . $niu . ".png";
		file_put_contents($fileName, $img);
		$projectId = 'sm-hackathon-project';

		$imageAnnotator = new ImageAnnotatorClient([
			'projectId' => $projectId
		]);

		// the name of the image file to annotate

		// prepare the image to be annotated
		$image = file_get_contents($fileName);

		// performs label detection on the image file
		$response = $imageAnnotator->objectLocalization($image);
		$objects = $response->getLocalizedObjectAnnotations();

		$person = "NO";
		$bool= false;
		foreach ($objects as $object) {
            		$name = $object->getName();
			if ($name == "Person") {$bool = true; $person = "SI"; }
			$score = $object->getScore();
    			$vertices = $object->getBoundingPoly()->getNormalizedVertices();
        	}

		if($bool === false) {	
			require_once __DIR__ .'/../models/getPresentials.php';
			$temp = getPresential($_SESSION['niu']);
			if($temp === 'started'){ putPresential($_SESSION['niu'], 'bot'); }
		}

        	$imageAnnotator->close();
		
        	return print_r($person);
    	}
?>
